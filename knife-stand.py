import cadquery as cq
import math

# All sizes in millimiters

# Thick plywood or board thickness
# this shoud be selected so that
# (SPACER_THICKNESS + PLATE_THICKNESS)*2 is
# grater than knife handle thickness measured
# across the blade
PLATE_THICKNESS     = 15

# Thin plywood or board thickness
# Should be thicker than blades with 
# some margin so that knives slide in and
# out without jamming. 4 mm is probably
# thicker than ideal but this is what I have.
SPACER_THICKNESS    = 4

# Roundover router bit radius
BASE_FILLET_R          = 6
# Blade length measured from handle with some margin
MAX_BLADE_LENGTH       = 210 + 10
# Blade width with some considerable margin
# (otherwise front hook that prevents knives to silde
# out on their own becomes too thin)
MAX_BLADE_WIDTH        = 45 + 25 

BACK_WALL_THICKNESS    = MAX_BLADE_WIDTH
HANDLE_REST_HEIGHT     = 50
BOTTOM_MIN_THICKNESS   = 15

# Slope added to prevent
# water accumulation inside the stand
BOTTOM_SLOPE_ANGLE_DEG = 3 
MAX_HANDLE_WIDTH       = 35 + 5
HOOK_HEIGHT            = BASE_FILLET_R*2 + 1

LARGE_FILLET_DIA  = (
    MAX_BLADE_WIDTH - MAX_HANDLE_WIDTH - BASE_FILLET_R - 1)

BOTTOM_SLOPE_ANGLE_RADIANS = math.radians(BOTTOM_SLOPE_ANGLE_DEG)
BOTTOM_MAX_THICKNESS = (
    BOTTOM_MIN_THICKNESS + 
    MAX_BLADE_WIDTH * math.tan(BOTTOM_SLOPE_ANGLE_RADIANS))
SHELF_HEIGHT = BOTTOM_MAX_THICKNESS + MAX_BLADE_LENGTH
TOTAL_HEIGHT = SHELF_HEIGHT + HANDLE_REST_HEIGHT
TOTAL_WIDTH  = BACK_WALL_THICKNESS + MAX_BLADE_WIDTH

HOOK_TOP     = SHELF_HEIGHT + HOOK_HEIGHT


def wall_plate_sketch():
    return (    
        cq.Sketch()
        .polygon(
            (
             (0,0),
             (0,TOTAL_HEIGHT),
             (BACK_WALL_THICKNESS,TOTAL_HEIGHT),
             (BACK_WALL_THICKNESS,SHELF_HEIGHT),
             (BACK_WALL_THICKNESS+MAX_HANDLE_WIDTH,SHELF_HEIGHT),
             (BACK_WALL_THICKNESS+MAX_HANDLE_WIDTH,HOOK_TOP),
             (TOTAL_WIDTH, HOOK_TOP),
             (TOTAL_WIDTH,0),
             (0,0))))

def spacer_sketch():
    return (
        cq.Sketch()
        .polygon(
            (
             (0, 0),
             (0, TOTAL_HEIGHT),
             (BACK_WALL_THICKNESS, TOTAL_HEIGHT),
             (BACK_WALL_THICKNESS, BOTTOM_MAX_THICKNESS),
             (TOTAL_WIDTH, BOTTOM_MIN_THICKNESS),
             (TOTAL_WIDTH,0),
             (0,0))))

def wall_plate_rounded_sketch():
    return (
        wall_plate_sketch()
        .edges()
        .vertices("<<Y")
        .fillet(BASE_FILLET_R)
        .reset()
        .edges()
        .vertices("<<X and >>Y")
        .fillet(LARGE_FILLET_DIA)
        .reset()
        .edges(">>X")
        .vertices(">>Y")
        .fillet(LARGE_FILLET_DIA)
        .reset()
        .edges("|X and >>Y")
        .vertices(">>Y")
        .fillet(BASE_FILLET_R)
        .reset()
        .edges("|Y")
        .vertices(">>Y[-3]")
        .fillet(BASE_FILLET_R)
        .reset()
        )

def spacer_rounded_sketch():
    return (
        spacer_sketch()
        .edges()
        .vertices("<<Y")
        .fillet(BASE_FILLET_R)
        .reset()
        .edges(">>Y")
        .vertices(">>X")
        .fillet(BASE_FILLET_R)
        .reset()
        .edges(">>Y")
        .vertices("<<X")
        .fillet(LARGE_FILLET_DIA))

def spacer():
    result = (
        cq.Workplane()
        .placeSketch(spacer_rounded_sketch())
        .extrude(SPACER_THICKNESS))
    
    result.faces("<Z").edges("<X").vertices("<Y").tag("con12")
    result.faces(">Z").edges("<X").vertices("<Y").tag("con11")
    
    result.faces("<Z").edges("<X").vertices(">Y").tag("con22")
    result.faces(">Z").edges("<X").vertices(">Y").tag("con21")

    
    return result

def side_plate_left():
    result = (
        cq.Workplane()
        .placeSketch(wall_plate_rounded_sketch())
        .extrude(PLATE_THICKNESS)
        .faces(">>Z")
        .fillet(BASE_FILLET_R))
        
    result.faces("<Z").edges("<X").vertices("<Y").tag("con12")
    result.faces("<Z").edges("<X").vertices(">Y").tag("con22")
    return result

def side_plate_right():
    result = (
        cq.Workplane()
        .placeSketch(wall_plate_rounded_sketch())
        .extrude(PLATE_THICKNESS)
        .faces("<<Z")
        .fillet(BASE_FILLET_R))    
    
    result.faces(">Z").edges("<X").vertices("<Y").tag("con11")
    result.faces(">Z").edges("<X").vertices(">Y").tag("con21")
    
    return result

def mid_plate():
    result = (
        cq.Workplane()
        .placeSketch(wall_plate_rounded_sketch())
        .extrude(PLATE_THICKNESS))
    
    result.faces("<Z").edges("<X").vertices("<Y").tag("con12")
    result.faces(">Z").edges("<X").vertices("<Y").tag("con11")
    
    result.faces("<Z").edges("<X").vertices(">Y").tag("con22")
    result.faces(">Z").edges("<X").vertices(">Y").tag("con21")
    
    return result

def knife_stand(nknives, n_spacers = 3):
    assembly = (
        cq.Assembly()
        .add(side_plate_left(), name="lplate", color=cq.Color("honeydew2"))
        .add(spacer(), name="spacer1", color=cq.Color("khaki3"))
        .constrain("spacer1?con11", "lplate?con12", "Point")
        .constrain("spacer1?con21", "lplate?con22", "Point")
        .constrain("spacer1@faces@>Z", "lplate@faces@<Z", "Axis"))
    prev_part_name = "spacer1"
    
    for i in range(1, nknives):
        for j in range(0,n_spacers):
            part_name = f"midplate{i+1}{j+1}"
            assembly.add(mid_plate(), name=part_name, color=cq.Color("lavenderblush3"))
            assembly.constrain(f"{part_name}?con11", f"{prev_part_name}?con12", "Point")
            assembly.constrain(f"{part_name}?con21", f"{prev_part_name}?con22", "Point")
            assembly.constrain(f"{part_name}@faces@>Z", f"{prev_part_name}@faces@<Z", "Axis")
            prev_part_name = part_name
        part_name = f"spaces{i+1}"
        assembly.add(spacer(), name=part_name, color=cq.Color("khaki3"))
        assembly.constrain(f"{part_name}?con11", f"{prev_part_name}?con12", "Point")
        assembly.constrain(f"{part_name}?con21", f"{prev_part_name}?con22", "Point")
        assembly.constrain(f"{part_name}@faces@>Z", f"{prev_part_name}@faces@<Z", "Axis")
        prev_part_name = part_name
        
    assembly.add(side_plate_right(), name="rplate", color=cq.Color("burlywood1"))
    assembly.constrain("rplate?con11", f"{prev_part_name}?con12", "Point")
    assembly.constrain("rplate?con21", f"{prev_part_name}?con22", "Point")
    assembly.constrain("rplate@faces@>Z", f"{prev_part_name}@faces@<Z", "Axis")
    
    assembly.solve()
    
    return assembly


show_object(knife_stand(3))

#show_object(side_plate_left())

#show_object(wall_plate_rounded_sketch())

#show_object(spacer_rounded_sketch())

#show_object(wall_plate_sketch())

#show_object(spacer_sketch())
