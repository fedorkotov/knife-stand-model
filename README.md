# cadquery-knife-stand

A parametric knife stand model for [CadQuery](https://github.com/CadQuery/cadquery) that can be made with simle tools from 15 mm and 4 mm plywood.

![3d view](3dview.png "Glued up stand")

## Parts

### Left side

![left side](left_plate.png "Left side")

### Thin spacer

![thin spacer](spacer.png "Thin spacer")

### Thick spacer

![thick spacer](mid_plate.png "Thick spacer")



### Right side

![right side](right_plate.png "Right side")
